/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
( function( $ ) {
	/* Header Image Logo */
	wp.customize( 'illdy_img_header_logo', function( value ) {
		value.bind( function( newval ) {
			if( newval !== '' ) {
				$( '#header .top-header .header-logo' ).empty();
				$( '#header .top-header .header-logo' ).prepend( '<img src="" alt="'+ wp.customize._value.illdy_text_logo +'" title="'+ wp.customize._value.illdy_text_logo +'" />' );
				$( '#header .top-header .header-logo img' ).attr( 'src', newval );
			} else {
				$( '#header .top-header .header-logo' ).text( wp.customize._value.illdy_text_logo() );
			}
		} );
	} );

	/* Company text logo */
	wp.customize( 'illdy_text_logo', function( value ) {
		value.bind( function( newval ) {
			if( wp.customize._value.illdy_img_header_logo() == '' ) {
				$( '#header .top-header .header-logo' ).html( newval );
			}
		} );
	} );

	/* Facebook URL */
	wp.customize( 'illdy_contact_bar_facebook_url', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-social a[title="Facebook"]' ).attr( 'href', newval );
		} );
	} );

	/* Twitter URL */
	wp.customize( 'illdy_contact_bar_twitter_url', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-social a[title="Twitter"]' ).attr( 'href', newval );
		} );
	} );

	/* LinkedIn URL */
	wp.customize( 'illdy_contact_bar_linkedin_url', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-social a[title="LinkedIn"]' ).attr( 'href', newval );
		} );
	} );

	/* email */
	wp.customize( 'illdy_email', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-right span a' ).attr( 'href', 'mailto: ' + newval );
			$( '#contact-us .section-content .contact-us-box .box-right span a' ).attr( 'title', newval );
			$( '#contact-us .section-content .contact-us-box .box-right span a' ).text( newval );
		} );
	} );

	/* phone number */
	wp.customize( 'illdy_phone', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-right span[data-customizer="contact-us-phone"]' ).attr( 'href', 'tel:' + newval );
			$( '#contact-us .section-content .contact-us-box .box-right span[data-customizer="contact-us-phone"]' ).attr( 'title', newval );
			$( '#contact-us .section-content .contact-us-box .box-right span[data-customizer="contact-us-phone"]' ).text( 'Phone: ' + newval );
		} );
	} );

	// Address 1
	wp.customize( 'illdy_address1', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-right span[data-customizer="contact-us-address-1"]' ).html( newval );
		} );
	} );

	// Address 1
	wp.customize( 'illdy_address2', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-right span[data-customizer="contact-us-address-2"]' ).html( newval );
		} );
	} );

	/* Footer Publicidade Image */
        wp.customize( 'illdy_img_footer_publicidade', function( value ) {
                value.bind( function( newval ) {
                        if( newval !== '' ) {
                                $( '#footer .footer-publicidade img' ).removeClass( 'customizer-display-none' );
                        } else {
                                $( '#footer .footer-publicidade img' ).addClass( 'customizer-display-none' );
                        }
                } );
        } );

        /* Footer Publicidade Url */
        wp.customize( 'illdy_footer_publicidade_url', function( value ) {
                value.bind( function( newval ) {
                        $( '#footer .footer-publicidade' ).attr( 'href', newval );
                } );
        } );

        /* Footer Publicidade */
        wp.customize( 'illdy_footer_publicidade', function( value ) {
                value.bind( function( newval ) {
                        $( '#footer .footer-publicidade-text' ).html( newval );
                } );
        } );

	/* Footer Image Logo */
	wp.customize( 'illdy_img_footer_logo', function( value ) {
		value.bind( function( newval ) {
			if( newval !== '' ) {
				$( '#footer .footer-logo img' ).removeClass( 'customizer-display-none' );
			} else {
				$( '#footer .footer-logo img' ).addClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Footer Copyright */
	wp.customize( 'illdy_footer_copyright', function( value ) {
		value.bind( function( newval ) {
			$( '#footer .copyright' ).html( newval );
		} );
	} );

	// Display theme copyright in the footer?
	wp.customize( 'illdy_general_footer_display_copyright', function( value ) {
		value.bind( function( newval ) {
			if( newval == true ) {
				$( '#footer .copyright span[data-customizer="copyright-credit"]' ).removeClass( 'customizer-display-none' );
			} else if( newval == false ) {
				$( '#footer .copyright span[data-customizer="copyright-credit"]' ).addClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Posted on on single blog posts */
	wp.customize( 'illdy_enable_post_posted_on_blog_posts', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .blog-post-meta' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .blog-post-meta' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Post Tags on single blog posts */
	wp.customize( 'illdy_enable_post_tags_blog_posts', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .blog-post-tags' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .blog-post-tags' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Post Comments on single blog posts */
	wp.customize( 'illdy_enable_post_comments_blog_posts', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .blog-post-meta .post-meta-comments' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .blog-post-meta .post-meta-comments' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );


	/* Author Info Box on single blog posts */
	wp.customize( 'illdy_enable_author_box_blog_posts', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .blog-post-author' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .blog-post-author' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Facebook visibility */
	wp.customize( 'illdy_facebook_visibility', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="facebook"]' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="facebook"]' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Twitter visibility */
	wp.customize( 'illdy_twitter_visibility', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="twitter"]' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="twitter"]' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* LinkedIN visibility */
	wp.customize( 'illdy_linkein_visibility', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="linkedin"]' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( 'body.single #blog .blog-post .social-links-list li[data-customizer="linkedin"]' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	/* Current header */
	wp.customize( 'header_image', function( value ) {
		value.bind( function( newval ) {
			if( newval == 'remove-header' ) {
				$( '#header.header-blog' ).removeAttr( 'style' );
			} else if( newval == 'random-uploaded-image' ) {
				// $( '#header.header-blog' ).removeAttr( 'style' );
			} else if( newval == 'random-default-image' ) {
				// $( '#header.header-blog' ).removeAttr( 'style' );
			} else {
				$( '#header.header-blog' ).css( 'background-image', 'url('+ newval +')' );
			}
		} );
	} );

	// Background color
	wp.customize( 'illdy_jumbotron_background_color', function( value ) {
		value.bind( function( newval ) {
			if( newval == '' ) {
				$( '#header.header-front-page' ).css( 'background-color', '');
			} else {
				$( '#header.header-front-page' ).css( 'background-color', newval );
			}
		} );
	} );

	// Image
	wp.customize( 'illdy_jumbotron_general_image', function( value ) {
		value.bind( function( newval ) {
			if( newval == '' ) {
				$( '#header.header-front-page' ).css( 'background-image', '');
			} else {
				$( '#header.header-front-page' ).css( 'background-image', 'url('+ newval +')' );
			}
		} );
	} );

	// Entry
	wp.customize( 'illdy_jumbotron_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#header .bottom-header .front-page-entry p' ).html( newval );
		} );
	} );

	// Bottom left text
	wp.customize( 'illdy_jumbotron_bottom_left_text', function( value ) {
		value.bind( function( newval ) {
			$( '#header .bottom-header .front-page-bottom-left.text p' ).html( newval );
		} );
	} );

	// Bottom left image


	// Has Modal Arvorismo
	wp.customize( 'illdy_jumbotron_modals_arvorismo', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#arvorismo' ).css( 'display', 'none' );
				jQuery( '#arvorismo-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#arvorismo' ).css( 'display', 'block' );
				jQuery( '#arvorismo-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Arvorismo Title
	wp.customize( 'illdy_jumbotron_modals_arvorismo_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#arvorismo-modal' ).modal( 'show' );
			$( '#header #arvorismo-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Arvorismo Content
	wp.customize( 'illdy_jumbotron_modals_arvorismo_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#arvorismo-modal' ).modal( 'show' );
			$( '#header #arvorismo-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Barco
	wp.customize( 'illdy_jumbotron_modals_barco', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#barco' ).css( 'display', 'none' );
				jQuery( '#barco-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#barco' ).css( 'display', 'block' );
				jQuery( '#barco-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Barco Title
	wp.customize( 'illdy_jumbotron_modals_barco_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#barco-modal' ).modal( 'show' );
			$( '#header #barco-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Barco Content
	wp.customize( 'illdy_jumbotron_modals_barco_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#barco-modal' ).modal( 'show' );
			$( '#header #barco-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Lancha
	wp.customize( 'illdy_jumbotron_modals_lancha', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#lancha' ).css( 'display', 'none' );
				jQuery( '#lancha-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#lancha' ).css( 'display', 'block' );
				jQuery( '#lancha-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Lancha Title
	wp.customize( 'illdy_jumbotron_modals_lancha_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#lancha-modal' ).modal( 'show' );
			$( '#header #lancha-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Lancha Content
	wp.customize( 'illdy_jumbotron_modals_lancha_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#lancha-modal' ).modal( 'show' );
			$( '#header #lancha-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Standup
	wp.customize( 'illdy_jumbotron_modals_standup', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#standup' ).css( 'display', 'none' );
				jQuery( '#standup-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#standup' ).css( 'display', 'block' );
				jQuery( '#standup-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Standup Title
	wp.customize( 'illdy_jumbotron_modals_standup_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#standup-modal' ).modal( 'show' );
			$( '#header #standup-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Standup Content
	wp.customize( 'illdy_jumbotron_modals_standup_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#standup-modal' ).modal( 'show' );
			$( '#header #standup-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Tirolesa
	wp.customize( 'illdy_jumbotron_modals_tirolesa', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#tirolesa' ).css( 'display', 'none' );
				jQuery( '#tirolesa-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#tirolesa' ).css( 'display', 'block' );
				jQuery( '#tirolesa-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Tirolesa Title
	wp.customize( 'illdy_jumbotron_modals_tirolesa_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#tirolesa-modal' ).modal( 'show' );
			$( '#header #tirolesa-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Tirolesa Content
	wp.customize( 'illdy_jumbotron_modals_tirolesa_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#tirolesa-modal' ).modal( 'show' );
			$( '#header #tirolesa-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Birdwatching
	wp.customize( 'illdy_jumbotron_modals_birdwatching', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#birdwatching' ).css( 'display', 'none' );
				jQuery( '#birdwatching-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#birdwatching' ).css( 'display', 'block' );
				jQuery( '#birdwatching-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Birdwatching Title
	wp.customize( 'illdy_jumbotron_modals_birdwatching_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#birdwatching-modal' ).modal( 'show' );
			$( '#header #birdwatching-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Birdwatching Content
	wp.customize( 'illdy_jumbotron_modals_birdwatching_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#birdwatching-modal' ).modal( 'show' );
			$( '#header #birdwatching-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Boia
	wp.customize( 'illdy_jumbotron_modals_boia', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#boia' ).css( 'display', 'none' );
				jQuery( '#boia-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#boia' ).css( 'display', 'block' );
				jQuery( '#boia-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Boia Title
	wp.customize( 'illdy_jumbotron_modals_boia_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#boia-modal' ).modal( 'show' );
			$( '#header #boia-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Boia Content
	wp.customize( 'illdy_jumbotron_modals_boia_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#boia-modal' ).modal( 'show' );
			$( '#header #boia-modal .modal-body' ).html( newval );
		} );
	} );


	// Has Modal Trekking
	wp.customize( 'illdy_jumbotron_modals_trekking', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#trekking' ).css( 'display', 'none' );
				jQuery( '#trekking-modal' ).modal( 'hide' );
			} else if( newval == true ) {
				$( '#trekking' ).css( 'display', 'block' );
				jQuery( '#trekking-modal' ).modal( 'show' );
			}
		} );
	} );

	// Modals Trekking Title
	wp.customize( 'illdy_jumbotron_modals_trekking_title', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#trekking-modal' ).modal( 'show' );
			$( '#header #trekking-modal h4.modal-title' ).html( newval );
		} );
	} );

	// Modals Trekking Content
	wp.customize( 'illdy_jumbotron_modals_trekking_content', function( value ) {
		value.bind( function( newval ) {
			jQuery( '#trekking-modal' ).modal( 'show' );
			$( '#header #trekking-modal .modal-body' ).html( newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_about_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#about' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#about' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_about_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#about.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_about_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#about.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_projects_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#projects' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#projects' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_projects_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#projects.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_projects_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#projects.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_testimonials_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#testimonials' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#testimonials' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_testimonials_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#testimonials.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Background Image
	wp.customize( 'illdy_testimonials_general_background_image', function( value ) {
		value.bind( function( newval ) {
			if( newval == '' ) {
				$( '#testimonials' ).removeAttr( 'style' );
			} else {
				$( '#testimonials' ).css( 'background-image', 'url('+ newval +')' );
			}
		} );
	} );

	// Show this section
	wp.customize( 'illdy_services_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#services' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#services' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_services_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#services.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_services_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#services.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_latest_news_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#latest-news' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#latest-news' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_latest_news_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#latest-news.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_latest_news_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#latest-news.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Button Text
	wp.customize( 'illdy_latest_news_button_text', function( value ) {
		value.bind( function( newval ) {
			$( '#latest-news .latest-news-button' ).html( '<i class="fa fa-chevron-circle-right"></i>' + newval );
		} );
	} );

	// Button URL
	wp.customize( 'illdy_latest_news_button_url', function( value ) {
		value.bind( function( newval ) {
			$( '#latest-news .latest-news-button' ).attr( 'href', newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_counter_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#counter' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#counter' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Type of Background
	wp.customize( 'illdy_counter_background_type', function( value ) {
		value.bind( function( newval ) {
			if( newval == 'image' ) {
				$( '#counter' ).css( 'background-color', '' );
				$( '#counter' ).css( 'background-image', 'url('+ wp.customize._value.illdy_counter_background_image() +')' );
			} else if( newval == 'color' ) {
				$( '#counter' ).css( 'background-image', '' );
				$( '#counter' ).css( 'background-color', 'color:' + wp.customize._value.illdy_counter_background_color() );
			}
		} );
	} );

	// Image
	wp.customize( 'illdy_counter_background_image', function( value ) {
		value.bind( function( newval ) {
			if( newval == '' ) {
				$( '#counter' ).removeAttr( 'style' );
			} else {
				$( '#counter' ).css( 'background-image', 'url('+ newval +')' );
			}
		} );
	} );

	// Color
	wp.customize( 'illdy_counter_background_color', function( value ) {
		value.bind( function( newval ) {
			$( '#counter' ).css( 'background-color', newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_team_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#team' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#team' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_team_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#team.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_team_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#team.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Show this section
	wp.customize( 'illdy_contact_us_general_show', function( value ) {
		value.bind( function( newval ) {
			if( newval == false ) {
				$( '#contact-us' ).addClass( 'customizer-display-none' );
			} else if( newval == true ) {
				$( '#contact-us' ).removeClass( 'customizer-display-none' );
			}
		} );
	} );

	// Title
	wp.customize( 'illdy_contact_us_general_title', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us.front-page-section .section-header h3' ).html( newval );
		} );
	} );

	// Entry
	wp.customize( 'illdy_contact_us_general_entry', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us.front-page-section .section-header p' ).html( newval );
		} );
	} );

	// Address Title
	wp.customize( 'illdy_contact_us_general_address_title', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-left[data-customizer="box-left-address-title"]' ).html( newval );
		} );
	} );

	// Customer Support Title
	wp.customize( 'illdy_contact_us_general_customer_support_title', function( value ) {
		value.bind( function( newval ) {
			$( '#contact-us .section-content .contact-us-box .box-left[data-customizer="box-left-customer-support-title"]' ).html( newval );
		} );
	} );
} )( jQuery );
