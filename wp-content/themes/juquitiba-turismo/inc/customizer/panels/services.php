<?php
// Set Panel ID
$panel_id = 'illdy_panel_services';

// Set prefix
$prefix = 'illdy';

/***********************************************/
/****************** SERVICES  ******************/
/***********************************************/
$wp_customize->add_section( $panel_id, array(
		'priority'       => 105,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __( 'Services Section', 'illdy' ),
		'description'    => __( 'Control various options for services section from front page.', 'illdy' ),
	) );

/***********************************************/
/******************* General *******************/
/***********************************************/


// Show this section
$wp_customize->add_setting( $prefix . '_services_general_show', array(
		'sanitize_callback' => $prefix . '_sanitize_checkbox',
		'default'           => 1,
		'transport'         => 'postMessage',
	) );
$wp_customize->add_control( $prefix . '_services_general_show', array(
		'type'     => 'checkbox',
		'label'    => __( 'Show this section?', 'illdy' ),
		'section'  => $panel_id,
		'priority' => 1,
	) );

// Title
$wp_customize->add_setting( $prefix . '_services_general_title', array(
		'sanitize_callback' => 'illdy_sanitize_html',
		'default'           => __( 'VAI UM PACOTE?', 'illdy' ),
		'transport'         => 'postMessage',
	) );
$wp_customize->add_control( $prefix . '_services_general_title', array(
		'label'       => __( 'Title', 'illdy' ),
		'description' => __( 'Add the title for this section.', 'illdy' ),
		'section'     => $panel_id,
		'priority'    => 2,
	) );

// Entry
if ( get_theme_mod( $prefix . '_services_general_entry' ) ) {

	$wp_customize->add_setting( $prefix . '_services_general_entry', array(
			'sanitize_callback' => 'illdy_sanitize_html',
			'default'           => __( 'Reserve já!', 'illdy' ),
			'transport'         => 'postMessage',
		) );
	$wp_customize->add_control( $prefix . '_services_general_entry', array(
			'label'       => __( 'Button text', 'illdy' ),
			'description' => __( 'Add the content for this section.', 'illdy' ),
			'section'     => $panel_id,
			'priority'    => 3,
			'type'        => 'input',
		) );

}elseif ( !defined( "ILLDY_COMPANION" ) ) {

    $wp_customize->add_setting(
        $prefix . '_services_entry_text',
        array(
            'sanitize_callback' => 'esc_html',
            'default'           => '',
            'transport'         => 'postMessage'
        )
    );
    $wp_customize->add_control(
        new Illdy_Text_Custom_Control(
            $wp_customize, $prefix . '_services_entry_text',
            array(
                'label'             => __( 'Install Illdy Companion', 'illdy' ),
                'description'       => sprintf(__( 'In order to edit description please install <a href="%s" target="_blank">Illdy Companion</a>', 'illdy' ), illdy_get_tgmpa_url()),
                'section'           => $panel_id,
                'settings'          => $prefix . '_services_entry_text',
                'priority'          => 3,
            )
        )
    );
}


// Service first
$wp_customize->add_setting( $prefix . '_services_first', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'Rafting+boat tour', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_first', array(
    'label'       => __( 'Título do serviço', 'illdy' ),
    'description' => __( 'Adicione o titulo do serviço.', 'illdy' ),
    'section'     => $panel_id,
) );
$wp_customize->add_setting( $prefix . '_services_first_price', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'R$ 175,00/pessoa', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_first_price', array(
    'label'       => __( 'Preço', 'illdy' ),
    'section'     => $panel_id,
    'type'        => 'textarea',
) );


// Service second
$wp_customize->add_setting( $prefix . '_services_second', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'Rafting+boat tour', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_second', array(
    'label'       => __( 'Título do serviço', 'illdy' ),
    'description' => __( 'Adicione o titulo do serviço.', 'illdy' ),
    'section'     => $panel_id,
) );
$wp_customize->add_setting( $prefix . '_services_second_price', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'R$ 175,00/pessoa', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_second_price', array(
    'label'       => __( 'Preço', 'illdy' ),
    'section'     => $panel_id,
    'type'        => 'textarea',
) );

// Service third
$wp_customize->add_setting( $prefix . '_services_third', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'Rafting+boat tour', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_third', array(
    'label'       => __( 'Título do serviço', 'illdy' ),
    'description' => __( 'Adicione o titulo do serviço.', 'illdy' ),
    'section'     => $panel_id,
) );
$wp_customize->add_setting( $prefix . '_services_third_price', array(
    'sanitize_callback' => 'illdy_sanitize_html',
    'default'           => __( 'R$ 175,00/pessoa', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_services_third_price', array(
    'label'       => __( 'Preço', 'illdy' ),
    'section'     => $panel_id,
    'type'        => 'textarea',
) );