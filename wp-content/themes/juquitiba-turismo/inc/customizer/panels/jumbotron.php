<?php

// Set prefix
$prefix = 'illdy';

$panel_id = 'illdy_panel_jumbotron_general';


/*************************************************/
/************** GENERAL JUMBOTRON  ***************/
/*************************************************/

$wp_customize->add_panel( $panel_id, array(
	'priority'       => 2,
	'title'          => __( 'Jumbotron Options', 'illdy' ),
	'description'    => __( 'You can change the header of this site layout', 'illdy' ),
) );


/*****************************************************/
/******************* Panel General *******************/
/*****************************************************/
$wp_customize->add_section( $prefix . '_jumbotron_general', array(
	'title'       => __( 'Panel General Section', 'illdy' ),
	'description' => __( 'Control various jumbotron related settings. Will only be displayed if a <strong>custom front-page is set in Settings -> Reading.</strong>', 'illdy' ),
	'priority'    => 100,
	'panel' => $panel_id
) );

// Entry
$wp_customize->add_setting( $prefix . '_jumbotron_general_entry', array(
	'sanitize_callback' => 'illdy_sanitize_html',
	'default'           => __( 'Navegue na imagem para conhecer os programas oferecidos.', 'illdy' ),
	'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_jumbotron_general_entry', array(
	'label'       => __( 'Info', 'illdy' ),
	'description' => __( 'The content added in this field will show in below the menu.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_general',
	'type'        => 'textarea',
) );


// Background Color
$wp_customize->add_setting( $prefix . '_jumbotron_background_color', array(
	'sanitize_callback' => 'sanitize_hex_color',
	'default'           => '#ffbe41',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $prefix . '_jumbotron_background_color', array(
	'label'       => __( 'Background color', 'illdy' ),
	'description' => __( 'Controls the background color for the container where the map is displayed on', 'illdy' ),
	'section'     => $prefix . '_jumbotron_general',
	'settings'    => $prefix . '_jumbotron_background_color'
) ) );

// Image
$wp_customize->add_setting( $prefix . '_jumbotron_general_image', array(
	'sanitize_callback' => 'esc_url_raw',
	'transport'         => 'postMessage',
	'default'			=> null
) );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, $prefix . '_jumbotron_general_image', array(
	'label'    => __( 'Background Image', 'illdy' ),
	'section'  => $prefix . '_jumbotron_general',
	'settings' => $prefix . '_jumbotron_general_image',
) ) );

// Featured image in header
$wp_customize->add_setting( $prefix . '_jumbotron_enable_featured_image', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 0,
	'transport'         => 'postMessage',
) );

$wp_customize->add_control( $prefix . '_jumbotron_enable_featured_image', array(
	'type'        => 'checkbox',
	'label'       => __( 'Featured image as jumbotron ?', 'illdy' ),
	'description' => __( 'This will remove the featured image from inside the post content and use it in the jumbotron as a background image. Works for single posts & pages.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_general',
) );

// Featured image in header
$wp_customize->add_setting( $prefix . '_jumbotron_enable_parallax_effect', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage',
) );

$wp_customize->add_control( $prefix . '_jumbotron_enable_parallax_effect', array(
	'type'        => 'checkbox',
	'label'       => __( 'Parallax effect on header image ?', 'illdy' ),
	'description' => __( 'Enabling this will add a parallax scrolling effect for the header image.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_general',
) );

/***************************************************/
/******************* Bottom Left *******************/
/***************************************************/
//$wp_customize->add_section( $prefix . '_jumbotron_bottom_left', array(
//	'title'       => __( 'Bottom Left Section', 'illdy' ),
//	'description' => __( 'Control various jumbotron related settings. Will only be displayed if a <strong>custom front-page is set in Settings -> Reading.</strong>', 'illdy' ),
//	'panel'       => $panel_id,
//	'priority'    => 101
//) );
//
//// Bottom left image
//$wp_customize->add_setting( $prefix . '_jumbotron_bottom_left_image', array(
//	'sanitize_callback' => 'esc_url_raw',
//	'default' 			=> esc_url_raw( get_stylesheet_directory_uri() . '/layout/images/masp.png' ),
//	'transport'         => 'postMessage',
//) );
//$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, $prefix . '_jumbotron_bottom_left_image', array(
//	'label'    => __( 'Image', 'illdy' ),
//	'section'  => $prefix . '_jumbotron_bottom_left',
//	'settings' => $prefix . '_jumbotron_bottom_left_image',
//) ) );
//
//
//// Bottom left text
//$wp_customize->add_setting( $prefix . '_jumbotron_bottom_left_text', array(
//	'sanitize_callback' => $prefix . '_sanitize_html',
//	'default'           => __( 'Translado opcional<br/>São Paulo - Juquitiba', 'illdy' ),
//	'transport'         => 'postMessage',
//) );
//$wp_customize->add_control( $prefix . '_jumbotron_bottom_left_text', array(
//	'label'       => __( 'Text', 'illdy' ),
//	'description' => __( 'The content added in this field will show in the rigth of image above.', 'illdy' ),
//	'section'     => $prefix . '_jumbotron_bottom_left',
//	'type'        => 'textarea',
//) );

/*************************************************/
/******************* INSTAGRAM *******************/
/*************************************************/
$wp_customize->add_section( $prefix . '_jumbotron_instagram', array(
    'title'       => __( 'Instagram Section', 'illdy' ),
    'description' => __( 'Control various jumbotron related settings. Will only be displayed if a <strong>custom front-page is set in Settings -> Reading.</strong>', 'illdy' ),
    'panel'       => $panel_id,
    'priority'    => 102
) );

// Widget textarea
$wp_customize->add_setting( $prefix . '_jumbotron_instagram_widget', array(
    'default'           => '<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/a7a08d1058095f88abdae6c77f2b6838.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>',
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_jumbotron_instagram_widget', array(
    'label'       => __( 'Light Widget', 'illdy' ),
    'description' => __( 'Get widget from lightwidget.com', 'illdy' ),
    'section'     => $prefix . '_jumbotron_instagram',
    'type'        => 'textarea',
) );

// Label
$wp_customize->add_setting( $prefix . '_jumbotron_instagram_label', array(
    'sanitize_callback' => $prefix . '_sanitize_html',
    'default'           => __( 'ACOMPANHE A @JUQUITIBATURIS', 'illdy' ),
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_jumbotron_instagram_label', array(
    'label'       => __( 'Instagram label', 'illdy' ),
    'section'     => $prefix . '_jumbotron_instagram',
    'type'        => 'textarea',
) );

// Link
$wp_customize->add_setting( $prefix . '_jumbotron_instagram_link', array(
    'sanitize_callback' => $prefix . '_sanitize_html',
    'default'           => 'https://www.instagram.com/juquitibaturis/',
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_jumbotron_instagram_link', array(
    'label'       => __( 'Instagram link', 'illdy' ),
    'section'     => $prefix . '_jumbotron_instagram',
    'type'        => 'textarea',
) );

// Link facebook
$wp_customize->add_setting( $prefix . '_jumbotron_facebook_link', array(
    'sanitize_callback' => $prefix . '_sanitize_html',
    'default'           => 'https://www.facebook.com/juquitibaturis/',
    'transport'         => 'postMessage',
) );
$wp_customize->add_control( $prefix . '_jumbotron_facebook_link', array(
    'label'       => __( 'Facebook link', 'illdy' ),
    'section'     => $prefix . '_jumbotron_instagram',
    'type'        => 'textarea',
) );

/**********************************************/
/******************* MODALS *******************/
/**********************************************/
$wp_customize->add_section( $prefix . '_jumbotron_modals', array(
	'title'       => __( 'Modals Section', 'illdy' ),
	'description' => __( 'Controle as informações dos botões e modais das imagens do header.', 'illdy' ),
	'panel'       => $panel_id,
	'priority'    => 103
) );

// Has Modal Arvorismo
$wp_customize->add_setting( $prefix . '_jumbotron_modals_arvorismo', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_arvorismo', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade arvorismo?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Arvorismo Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_arvorismo_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Arvorismo no Sítio Canoar', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_arvorismo_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal arvorismo.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Arvorismo Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_arvorismo_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '68,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_arvorismo_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade arvorismo.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Arvorismo Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_arvorismo_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'arvorismo-sitio-canoar', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_arvorismo_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade arvorismo.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Arvorismo Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_arvorismo_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_arvorismo_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal arvorismo.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Barco
$wp_customize->add_setting( $prefix . '_jumbotron_modals_barco', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_barco', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade rafting?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Barco Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_barco_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_barco_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal rafting.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Barco Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_barco_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_barco_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade rafting.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Barco Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_barco_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_barco_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade rafting.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Barco Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_barco_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_barco_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal rafting.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Lancha
$wp_customize->add_setting( $prefix . '_jumbotron_modals_lancha', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_lancha', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade boat tour?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Lancha Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_lancha_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Boat tour na represa', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_lancha_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal boat tour.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Lancha Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_lancha_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '89,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_lancha_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade boat tour.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Lancha Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_lancha_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'boat-tour-represa', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_lancha_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade boat tour.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Lancha Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_lancha_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_lancha_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal boat tour.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Standup
$wp_customize->add_setting( $prefix . '_jumbotron_modals_standup', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_standup', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade standup?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Standup Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_standup_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Standup paddle no Juquiá', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_standup_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal standup.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Standup Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_standup_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_standup_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade standup.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Standup Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_standup_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_standup_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade standup.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Standup Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_standup_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Em breve', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_standup_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal standup.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Tirolesa
$wp_customize->add_setting( $prefix . '_jumbotron_modals_tirolesa', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_tirolesa', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade tirolesa?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Tirolesa Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_tirolesa_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Tirolesa no Sítio Canoar', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_tirolesa_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal tirolesa.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Tirolesa Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_tirolesa_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '40,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_tirolesa_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade tirolesa.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Tirolesa Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_tirolesa_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'tirolesa-sitio-canoar', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_tirolesa_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade tirolesa.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Tirolesa Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_tirolesa_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_tirolesa_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal tirolesa.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Birdwatching
$wp_customize->add_setting( $prefix . '_jumbotron_modals_birdwatching', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_birdwatching', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade birdwatching?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Birdwatching Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_birdwatching_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Birdwatching no Espinheiro', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_birdwatching_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal birdwatching.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Birdwatching Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_birdwatching_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '90,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_birdwatching_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade birdwatching.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Birdwatching Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_birdwatching_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'birdwatching-espinheiro', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_birdwatching_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade birdwatching.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Birdwatching Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_birdwatching_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_birdwatching_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal birdwatching.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Boia
$wp_customize->add_setting( $prefix . '_jumbotron_modals_boia', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_boia', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade boia?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Boia Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_boia_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Boia cross na Pauleira', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_boia_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal boia.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Boia Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_boia_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_boia_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade boia.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Boia Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_boia_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_boia_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade boia.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Boia Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_boia_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Em breve', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_boia_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal boia.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );


// Has Modal Trekking
$wp_customize->add_setting( $prefix . '_jumbotron_modals_trekking', array(
	'sanitize_callback' => $prefix . '_sanitize_checkbox',
	'default'           => 1,
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_trekking', array(
	'type'        => 'checkbox',
	'label'       => __( 'Deseja ativar a atividade trekking?', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals'
) );

// Modal Trekking Title
$wp_customize->add_setting( $prefix . '_jumbotron_modals_trekking_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_trekking_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'description' => __( 'Digite o título do modal trekking.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Trekking Price
$wp_customize->add_setting( $prefix . '_jumbotron_modals_trekking_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_trekking_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'description' => __( 'Digite o preço da atividade trekking.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Trekking Link
$wp_customize->add_setting( $prefix . '_jumbotron_modals_trekking_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_trekking_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'description' => __( 'Digite o link da atividade trekking.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'input'
) );

// Modal Trekking Content
$wp_customize->add_setting( $prefix . '_jumbotron_modals_trekking_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_modals_trekking_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'description' => __( 'Digite o conteúdo do modal trekking.', 'illdy' ),
	'section'     => $prefix . '_jumbotron_modals',
	'type'        => 'textarea'
) );

/*********************************************************/
/******************* SUBCONTENT MODALS *******************/
/*********************************************************/
$wp_customize->add_section( $prefix . '_jumbotron_submodals', array(
	'title'       => __( 'Subcontent Modals Section', 'illdy' ),
	'description' => __( 'Adicione opções extras nos modais.', 'illdy' ),
	'panel'       => $panel_id,
	'priority'    => 104
) );

// Subcontent 1
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_1', array(
    'sanitize_callback' => $prefix . '_sanitize_select',
    'default'           => 2
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_1', array(
    'label'         => __( 'First subcontent', 'illdy' ),
    'description'   => __( 'Selecione o modal pai do subconteúdo.', 'illdy' ),
    'type'          => 'select',
    'section'       => $prefix . '_jumbotron_submodals',
    'choices'       => array(
        0   => __( 'Selecione uma opção', 'illdy' ),
        1   => __( 'Arvorismo', 'illdy' ),
        2   => __( 'Rafting', 'illdy' ),
        3   => __( 'Lancha', 'illdy' ),
        4   => __( 'Standup', 'illdy' ),
        5   => __( 'Tirolesa', 'illdy' ),
        6   => __( 'Birdwatching', 'illdy' ),
        7   => __( 'Boia', 'illdy' ),
        8   => __( 'Trekking', 'illdy' )
    )
) );

// Subcontent 1 Title
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_1_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Rafting no Juquiá', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_1_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 1 Price
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_1_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '99,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_1_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 1 Link
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_1_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'rafting-juquia', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_1_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 1 Content
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_1_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_1_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'textarea'
) );

// Subcontent 2
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_2', array(
    'sanitize_callback' => $prefix . '_sanitize_select',
    'default'           => 2
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_2', array(
    'label'         => __( 'Second subcontent', 'illdy' ),
    'description'   => __( 'Selecione o modal pai do subconteúdo.', 'illdy' ),
    'type'          => 'select',
    'section'       => $prefix . '_jumbotron_submodals',
    'choices'       => array(
        0   => __( 'Selecione uma opção', 'illdy' ),
        1   => __( 'Arvorismo', 'illdy' ),
        2   => __( 'Rafting', 'illdy' ),
        3   => __( 'Lancha', 'illdy' ),
        4   => __( 'Standup', 'illdy' ),
        5   => __( 'Tirolesa', 'illdy' ),
        6   => __( 'Birdwatching', 'illdy' ),
        7   => __( 'Boia', 'illdy' ),
        8   => __( 'Trekking', 'illdy' )
    )
) );

// Subcontent 2 Title
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_2_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Rafting no Alto Juquiá', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_2_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 2 Price
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_2_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '122,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_2_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 2 Link
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_2_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'rafting-alto-juquia', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_2_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 2 Content
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_2_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_2_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'textarea'
) );

// Subcontent 3
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_3', array(
    'sanitize_callback' => $prefix . '_sanitize_select',
    'default'           => 2
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_3', array(
    'label'         => __( 'Third subcontent', 'illdy' ),
    'description'   => __( 'Selecione o modal pai do subconteúdo.', 'illdy' ),
    'type'          => 'select',
    'section'       => $prefix . '_jumbotron_submodals',
    'choices'       => array(
        0   => __( 'Selecione uma opção', 'illdy' ),
        1   => __( 'Arvorismo', 'illdy' ),
        2   => __( 'Rafting', 'illdy' ),
        3   => __( 'Lancha', 'illdy' ),
        4   => __( 'Standup', 'illdy' ),
        5   => __( 'Tirolesa', 'illdy' ),
        6   => __( 'Birdwatching', 'illdy' ),
        7   => __( 'Boia', 'illdy' ),
        8   => __( 'Trekking', 'illdy' )
    )
) );

// Subcontent 3 Title
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_3_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Rafting Noturno', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_3_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 3 Price
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_3_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '122,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_3_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 3 Link
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_3_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'rafting-noturno', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_3_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 3 Content
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_3_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_3_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'textarea'
) );

// Subcontent 4
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_4', array(
    'sanitize_callback' => $prefix . '_sanitize_select',
    'default'           => 8
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_4', array(
    'label'         => __( 'Fourth subcontent', 'illdy' ),
    'description'   => __( 'Selecione o modal pai do subconteúdo.', 'illdy' ),
    'type'          => 'select',
    'section'       => $prefix . '_jumbotron_submodals',
    'choices'       => array(
        0   => __( 'Selecione uma opção', 'illdy' ),
        1   => __( 'Arvorismo', 'illdy' ),
        2   => __( 'Rafting', 'illdy' ),
        3   => __( 'Lancha', 'illdy' ),
        4   => __( 'Standup', 'illdy' ),
        5   => __( 'Tirolesa', 'illdy' ),
        6   => __( 'Birdwatching', 'illdy' ),
        7   => __( 'Boia', 'illdy' ),
        8   => __( 'Trekking', 'illdy' )
    )
) );

// Subcontent 4 Title
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_4_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Trekking no Juquiá', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_4_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 4 Price
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_4_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( '54,00', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_4_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 4 Link
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_4_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'trekking-juquia', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_4_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 4 Content
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_4_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_4_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'textarea'
) );

// Subcontent 5
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_5', array(
    'sanitize_callback' => $prefix . '_sanitize_select',
    'default'           => 8
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_5', array(
    'label'         => __( 'Fifth subcontent', 'illdy' ),
    'description'   => __( 'Selecione o modal pai do subconteúdo.', 'illdy' ),
    'type'          => 'select',
    'section'       => $prefix . '_jumbotron_submodals',
    'choices'       => array(
        0   => __( 'Selecione uma opção', 'illdy' ),
        1   => __( 'Arvorismo', 'illdy' ),
        2   => __( 'Rafting', 'illdy' ),
        3   => __( 'Lancha', 'illdy' ),
        4   => __( 'Standup', 'illdy' ),
        5   => __( 'Tirolesa', 'illdy' ),
        6   => __( 'Birdwatching', 'illdy' ),
        7   => __( 'Boia', 'illdy' ),
        8   => __( 'Trekking', 'illdy' )
    )
) );

// Subcontent 5 Title
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_5_title', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Trekking no Observatório', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_5_title', array(
	'label'       => __( 'Título', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 5 Price
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_5_price', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_5_price', array(
	'label'       => __( 'Preço', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 5 Link
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_5_link', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => '',
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_5_link', array(
	'label'       => __( 'Link', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'input'
) );

// Subcontent 5 Content
$wp_customize->add_setting( $prefix . '_jumbotron_submodals_5_content', array(
	'sanitize_callback' => $prefix . '_sanitize_html',
	'default'           => __( 'Em breve', 'illdy' ),
	'transport'         => 'postMessage'
) );
$wp_customize->add_control( $prefix . '_jumbotron_submodals_5_content', array(
	'label'       => __( 'Conteúdo', 'illdy' ),
	'section'     => $prefix . '_jumbotron_submodals',
	'type'        => 'textarea'
) );