<?php
/**
 *  Customizer
 */
if ( ! function_exists( 'illdy_customize_register' ) ) {
	function illdy_customize_register( $wp_customize ) {


		// Get Settings
		$wp_customize->get_setting( 'blogname' )->transport          = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport   = 'postMessage';
		$wp_customize->get_setting( 'header_image' )->transport      = 'postMessage';
		$wp_customize->get_setting( 'header_image_data' )->transport = 'postMessage';

		// background-color: #ffbe41; amarelo
		// color: #257a10; verde
		/**********************************************/
		/*************** INIT ************************/
		/**********************************************/

		// Custom Controls
		require_once get_template_directory() . '/inc/customizer/custom-controls.php';

		// General Options
		require_once get_stylesheet_directory() . '/inc/customizer/panels/general-options.php';

		// Blog Options
		require_once get_template_directory() . '/inc/customizer/panels/blog-options.php';

		// Jumbotron
		require_once get_stylesheet_directory() . '/inc/customizer/panels/jumbotron.php';

		// Sections Order
		require_once get_stylesheet_directory() . '/inc/customizer/panels/sections-order.php';

		// About
		require_once get_stylesheet_directory() . '/inc/customizer/panels/about.php';

		// Testimonials
		require_once get_stylesheet_directory() . '/inc/customizer/panels/testimonials.php';

		// Projects
		require_once get_stylesheet_directory() . '/inc/customizer/panels/projects.php';

		// Services
		require_once get_stylesheet_directory() . '/inc/customizer/panels/services.php';

		// Latest News
		require_once get_stylesheet_directory() . '/inc/customizer/panels/latest-news.php';

		// Counter
		require_once get_stylesheet_directory() . '/inc/customizer/panels/counter.php';

		// Team
		require_once get_stylesheet_directory() . '/inc/customizer/panels/team.php';

		// Contact Us
		require_once get_stylesheet_directory() . '/inc/customizer/panels/contact-us.php';
	}

	add_action( 'customize_register', 'illdy_customize_register' );
}

/**
 *  Customizer Live Preview
 */
if ( ! function_exists( 'illdy_customizer_live_preview' ) ) {
	add_action( 'customize_preview_init', 'illdy_customizer_live_preview' );

	function illdy_customizer_live_preview() {
		wp_enqueue_script( 'illdy-customizer-live-preview', get_stylesheet_directory_uri() . '/inc/customizer/assets/js/illdy-customizer-live-preview.js', array( 'customize-preview' ), '1.0', true );
	}

}