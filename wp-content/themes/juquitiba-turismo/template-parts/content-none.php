<?php
/**
 *	Template part for displaying a message that posts cannot be found.
 *
 *	@package WordPress
 *	@subpackage illdy
 */
?>
<article <?php post_class( 'blog-post' ); ?>>
    <a href="<?php echo esc_url( home_url() ); ?>" title="<?php _e( 'Nada encontrado', 'illdy' ); ?>" class="blog-post-title"><?php _e( 'Nada encontrado', 'illdy' ); ?></a>
</article><!--/#post-<?php the_ID(); ?>.blog-post-->