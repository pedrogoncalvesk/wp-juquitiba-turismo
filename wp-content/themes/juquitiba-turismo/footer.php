<?php
/**
 *    The template for dispalying the footer.
 *
 * @package    WordPress
 * @subpackage illdy
 */
?>
<?php

$footer_copyright  = get_theme_mod( 'illdy_footer_copyright', __( '&copy; Copyright 2016. Todos os direitos reservados.', 'illdy' ) );
$img_footer_logo   = get_theme_mod( 'illdy_img_footer_logo', esc_url( get_stylesheet_directory_uri() . '/layout/images/footer-logo.png' ) );

$footer_publicidade = get_theme_mod( 'illdy_footer_publicidade', __( 'Publicidade', 'illdy' ) );
$footer_publicidade_url = get_theme_mod( 'illdy_footer_publicidade_url', esc_url( 'http://juquitiba-sp.com/' ) );
$img_footer_publicidade = get_theme_mod( 'illdy_img_footer_publicidade', esc_url( get_stylesheet_directory_uri() . '/layout/images/bg-publicidade.png' ) );

$columns = array(3,3,3);
$offset = false;

if (!is_active_sidebar( 'footer-sidebar-1' ) && !is_active_sidebar( 'footer-sidebar-2' ) && is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 0;
    $columns[1] = 0;
    $columns[2] = 9;
} else if(is_active_sidebar( 'footer-sidebar-1' ) && !is_active_sidebar( 'footer-sidebar-2' ) && is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[1] = 0;
    $columns[2] = 6;
} else if(!is_active_sidebar( 'footer-sidebar-1' ) && is_active_sidebar( 'footer-sidebar-2' ) && is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 0;
    $columns[2] = 6;
} else if(!is_active_sidebar( 'footer-sidebar-1' ) && is_active_sidebar( 'footer-sidebar-2' ) && !is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 0;
    $columns[1] = 9;
    $columns[2] = 0;
} else if(is_active_sidebar( 'footer-sidebar-1' ) && is_active_sidebar( 'footer-sidebar-2' ) && !is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[1] = 6;
    $columns[2] = 0;
} else if(!is_active_sidebar( 'footer-sidebar-1' ) && is_active_sidebar( 'footer-sidebar-2' ) && is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 0;
    $columns[2] = 6;
} else if(is_active_sidebar( 'footer-sidebar-1' ) && !is_active_sidebar( 'footer-sidebar-2' ) && !is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 9;
    $columns[1] = 0;
    $columns[2] = 0;
} else if(is_active_sidebar( 'footer-sidebar-1' ) && is_active_sidebar( 'footer-sidebar-2' ) && !is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[1] = 6;
    $columns[2] = 0;
} else if(is_active_sidebar( 'footer-sidebar-1' ) && !is_active_sidebar( 'footer-sidebar-2' ) && is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[1] = 0;
    $columns[2] = 6;
} else if(!is_active_sidebar( 'footer-sidebar-1' ) && !is_active_sidebar( 'footer-sidebar-2' ) && !is_active_sidebar( 'footer-sidebar-3' )) {
    $columns[0] = 0;
    $columns[1] = 0;
    $columns[2] = 0;
    $offset = true;
}
?>
<footer id="footer">
	<div class="container">
		<div class="row">
            <?php if ($columns[0] !== 0): ?>
                <div class="col-sm-<?php echo $columns[0] ?>">
                    <?php
                    if ( is_active_sidebar( 'footer-sidebar-1' ) ):
                        dynamic_sidebar( 'footer-sidebar-1' );
                    endif;
                    ?>
                </div><!--/.col-sm-3-->
            <?php endif; ?>
            <?php if ($columns[1] !== 0): ?>
                <div class="col-sm-<?php echo $columns[1] ?>">
                    <?php
                    if ( is_active_sidebar( 'footer-sidebar-2' ) ):
                        dynamic_sidebar( 'footer-sidebar-2' );
                    endif;
                    ?>
                </div><!--/.col-sm-3-->
            <?php endif; ?>
            <?php if ($columns[2] !== 0): ?>
                <div class="col-sm-<?php echo $columns[2] ?>">
                    <?php
                    if ( is_active_sidebar( 'footer-sidebar-3' ) ):
                        dynamic_sidebar( 'footer-sidebar-3' );
                    endif;
                    ?>
                </div><!--/.col-sm-3-->
            <?php endif; ?>
            <div class="col-sm-3 <?php echo ($offset === true) ? 'col-sm-offset-9' : ''; ?>">
                <?php if ( $img_footer_publicidade ): ?>
                    <span class="footer-publicidade-text"><?php echo illdy_sanitize_html( $footer_publicidade ); ?></span>
                    <a href="<?php echo esc_url( $footer_publicidade_url ); ?>" target="_blank" title="<?php echo illdy_sanitize_html( $footer_publicidade ); ?>" class="footer-publicidade">
                        <img src="<?php echo esc_url( $img_footer_publicidade ); ?>" alt="<?php echo illdy_sanitize_html( $footer_publicidade ); ?>" title="<?php echo illdy_sanitize_html( $footer_publicidade ); ?>" />
                    </a>
                <?php endif; ?>
		<?php if ( $img_footer_logo ): ?>
                    <a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="footer-logo"><img src="<?php echo esc_url( $img_footer_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" /></a>
                <?php endif; ?>
                <p class="copyright">
                    <span data-customizer="copyright-credit"><?php printf( 'by <a href="%s" title="%s" target="_blank">%s</a>.', esc_url( 'http://pedropereira.me' ), __( 'Pedro Pereira', 'illdy' ), __( 'Pedro Pereira', 'illdy' ) ); ?></span>
                </p>
                <span><?php echo illdy_sanitize_html( $footer_copyright ); ?></span>
            </div><!--/.col-sm-3-->
		</div><!--/.row-->
	</div><!--/.container-->
</footer><!--/#footer-->
<?php wp_footer(); ?>
</body>
</html>