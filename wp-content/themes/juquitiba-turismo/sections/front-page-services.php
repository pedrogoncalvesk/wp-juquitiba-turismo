<?php
/**
 *	The template for displaying services section in front page.
 *
 *	@package WordPress
 *	@subpackage illdy
 */
?>
<?php 
$services_general_title 	= get_theme_mod( 'illdy_services_general_title', __( 'Vai um pacote?', 'illdy' ) );
$services_general_entry 	= get_theme_mod( 'illdy_services_general_entry', __( 'Reserve já!', 'illdy' ) );
$services_image_background	= get_theme_mod( 'illdy_services_general_background_image', esc_url( get_stylesheet_directory_uri() . '/layout/images/services-background.png' ) );

$services = array();

$services_qtd = 12;

$first = get_theme_mod( 'illdy_services_first', __( 'Rafting+boat tour', 'illdy' ) );
if ( !empty( $first ) ) {
    $services[] = array(
        'title' 	=> get_theme_mod( 'illdy_services_first', __( 'Rafting+boat tour', 'illdy' ) ),
        'price'		=> get_theme_mod( 'illdy_services_first_price', __( 'R$ 175,00/pessoa', 'illdy' ) )
    );
}
$second = get_theme_mod( 'illdy_services_second', __( 'Rafting+boat tour', 'illdy' ) );
if ( !empty( $second ) ) {
    $services[] = array(
        'title' 	=> get_theme_mod( 'illdy_services_second', __( 'Rafting+boat tour', 'illdy' ) ),
        'price'		=> get_theme_mod( 'illdy_services_second_price', __( 'R$ 175,00/pessoa', 'illdy' ) )
    );
}
$third = get_theme_mod( 'illdy_services_third', __( 'Rafting+boat tour', 'illdy' ) );
if ( !empty( $third ) ) {
    $services[] = array(
        'title' 	=> get_theme_mod( 'illdy_services_third', __( 'Rafting+boat tour', 'illdy' ) ),
        'price'		=> get_theme_mod( 'illdy_services_third_price', __( 'R$ 175,00/pessoa', 'illdy' ) )
    );
}

$services_qtd /= sizeof($services);
?>

<?php if ( $services_general_title != '' || $services_general_entry != '' ) { ?>

<section id="services" class="front-page-section" style="background-image: url(<?php echo $services_image_background; ?>);">
	<?php if( $services_general_title ): ?>
		<div class="section-header">
			<div class="container">
				<div class="row">
					<?php if( $services_general_title ): ?>
						<div class="col-sm-12">
							<h3><?php echo illdy_sanitize_html( $services_general_title ); ?></h3>
						</div><!--/.col-sm-12-->
					<?php endif; ?>
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!--/.section-header-->
	<?php endif; ?>
	<div class="section-content">
		<div class="container">
			<div class="row">
                <?php foreach($services as $service): ?>
                    <div class="col-sm-<?php echo $services_qtd; ?>">
                        <div class="service">
                            <h2><?php echo $service['title']; ?></h2>
                            <div class="price">
                                <?php echo $service['price']; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
			</div><!--/.row-->
			<!-- <div class="row">
				<button><?php echo $services_general_entry; ?></button>
			</div> -->
		</div><!--/.container-->
	</div><!--/.section-content-->
</section><!--/#services.front-page-section-->

<?php } ?>