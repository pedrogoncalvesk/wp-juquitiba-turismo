<?php
/**
 *    The template for displaying the bottom header section in front page.
 *
 * @package    WordPress
 * @subpackage illdy
 */

use JangoBrick\SVG\SVGImage;

include 'arvorismo-barco.php';
include 'tirolesa-lancha.php';
include 'trekking-boia.php';

$entry                 = get_theme_mod( 'illdy_jumbotron_general_entry', __( 'Navegue na imagem para conhecer os programas oferecidos.' ) );

// MODALS
$modals = array();

$modals[] = array(
	'order' 	=> 1,
	'key' 		=> 'arvorismo',
	'has_modal' => get_theme_mod( 'illdy_jumbotron_modals_arvorismo', true ),
	'title' 	=> get_theme_mod( 'illdy_jumbotron_modals_arvorismo_title', __( 'Arvorismo no Sítio Canoar', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_arvorismo_price', __( '68,00', 'illdy' ) ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_arvorismo_link', __( 'arvorismo-sitio-canoar', 'illdy' ) ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_arvorismo_content', '' )
);

$modals[] = array(
	'order'		=> 2,
	'key'		=> 'barco',
	'has_modal'	=> get_theme_mod( 'illdy_jumbotron_modals_barco', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_barco_title', '' ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_barco_price', '' ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_barco_link', '' ),
	'content' 	=> get_theme_mod( 'illdy_jumbotron_modals_barco_content', '' )
);

$modals[] = array(
	'order' 	=> 3,
	'key'		=> 'lancha',
	'has_modal'	=> get_theme_mod( 'illdy_jumbotron_modals_lancha', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_lancha_title', __( 'Boat tour na represa', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_lancha_price', __( '89,00', 'illdy' ) ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_lancha_link', __( 'boat-tour-represa', 'illdy' ) ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_lancha_content', '' )
);

$modals[] = array(
	'order' 	=> 4,
	'key'		=> 'standup',
	'has_modal' => get_theme_mod( 'illdy_jumbotron_modals_standup', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_standup_title', __( 'Standup paddle no Juquiá', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_standup_price', '' ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_standup_link', '' ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_standup_content', __('Em breve', 'illdy') )
);

$modals[] = array(
	'order' 	=> 5,
	'key'		=> 'tirolesa',
	'has_modal' => get_theme_mod( 'illdy_jumbotron_modals_tirolesa', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_tirolesa_title', __( 'Tirolesa no Sítio Canoar', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_tirolesa_price', __( '40,00', 'illdy' ) ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_tirolesa_link', __( 'tirolesa-sitio-canoar', 'illdy' ) ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_tirolesa_content', '' )
);

$modals[] = array(
	'order' 	=> 6,
	'key' 		=> 'birdwatching',
	'has_modal'	=> get_theme_mod( 'illdy_jumbotron_modals_birdwatching', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_birdwatching_title', __( 'Birdwatching no Espinheiro', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_birdwatching_price', __( '90,00', 'illdy' ) ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_birdwatching_link', __( 'birdwatching-espinheiro', 'illdy' ) ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_birdwatching_content', '' )
);

$modals[] = array(
	'order' 	=> 7,
	'key' 		=> 'boia',
	'has_modal'	=> get_theme_mod( 'illdy_jumbotron_modals_boia', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_boia_title', __( 'Boia cross na Pauleira', 'illdy' ) ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_boia_price', '' ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_boia_link', '' ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_boia_content', __( 'Em breve', 'illdy' ) )
);

$modals[] = array(
	'order' 	=> 8,
	'key' 		=> 'trekking',
	'has_modal'	=> get_theme_mod( 'illdy_jumbotron_modals_trekking', true ),
	'title'		=> get_theme_mod( 'illdy_jumbotron_modals_trekking_title', '' ),
	'price'		=> get_theme_mod( 'illdy_jumbotron_modals_trekking_price', '' ),
	'link'		=> get_theme_mod( 'illdy_jumbotron_modals_trekking_link', '' ),
	'content'	=> get_theme_mod( 'illdy_jumbotron_modals_trekking_content', '' )
);


$style = '<style type="text/css">';
foreach ($modals as $modal) {
	if ($modal[ 'has_modal' ] == false) {
		$style .= '#header .front-page #' . $modal['key'] . '{display: none;}';
		$style .= '#header .front-page #circle-' . $modal['key'] . '{cursor: default;}';
	}
}
$style .= '</style>';


// INFO LOCATED IN BOTTOM LEFT
$bottom_left_image 	   = get_theme_mod( 'illdy_jumbotron_bottom_left_image', esc_url( get_stylesheet_directory_uri() . '/layout/images/masp.png' ) );
$bottom_left_text 	   = get_theme_mod( 'illdy_jumbotron_bottom_left_text', __( 'Translado opcional<br/>São Paulo - Juquitiba', 'illdy' ) );

$instagram_widget      = get_theme_mod( 'illdy_jumbotron_instagram_widget' );
$instagram_label       = get_theme_mod( 'illdy_jumbotron_instagram_label', __( 'ACOMPANHE A @JUQUITIBATURIS', 'illdy' ) );
$instagram_link        = get_theme_mod( 'illdy_jumbotron_instagram_link', 'https://www.instagram.com/juquitibaturis/' );
$facebook_link         = get_theme_mod( 'illdy_jumbotron_facebook_link', 'https://www.facebook.com/juquitibaturis/' );

// SUBCONTENT MODALS
$submodals = juquitiba_turismo_subcontent_modals_order( 'illdy_jumbotron_submodals_' );


if ( $entry ):
	?>
	
	<?php echo $style; ?>
	<div class="bottom-header front-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-10 col-sm-2 col-sm-offset-10 front-page-entry">
					<?php if ( $entry ): ?>
						<p><?php echo illdy_sanitize_html( $entry ); ?></p>
					<?php endif; ?>
				</div>
				<?php if ( isset($arvorismo_barco) || isset($tirolesa_lancha) || isset($trekking_boia) ): ?>
					<div class="col-xs-11 col-sm-10 col-md-9 col-centered">
						<div class="col-sm-4 front-page-columns">
                            <?php echo SVGImage::fromString( $arvorismo_barco ); ?>
						</div>
						<div class="col-sm-4 front-page-columns">
                            <?php echo SVGImage::fromString( $tirolesa_lancha ); ?>
						</div>
						<div class="col-sm-4 front-page-columns">
                            <?php echo SVGImage::fromString( $trekking_boia ); ?>
						</div>
					</div>
				<?php endif; ?>
			</div><!--/.row-->
		</div><!--/.container-->
	</div><!--/.bottom-header.front-page-->

    <div class="instagram-feed" data-label="<?php echo isset($instagram_label) ? $instagram_label : ''; ?>">
        <a href="<?php echo isset($facebook_link) ? $facebook_link : '#'; ?>" target="_blank" class="stack-facebook">
            <span class="fa-stack fa-lg ">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
            </span>
        </a>

        <a href="<?php echo isset($instagram_link) ? $instagram_link : '#'; ?>" target="_blank" class="stack-instagram">
            <span class="fa-stack fa-lg ">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
            </span>
        </a>

        <?php if ( $instagram_widget ): ?>
            <?php echo $instagram_widget ?>
        <?php endif; ?>
    </div>

	<?php foreach ($modals as $modal): ?>
		<?php if ( $modal['has_modal'] == true || isset( $submodals[$modal['order']] ) ): ?>
			<div id="<?php echo $modal['key']; ?>-modal" class="activities modal fade" tabindex="-1" role="dialog" aria-labelledby="<?php echo $modal['key']; ?>Modal">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
						<div class="modal-body">
							<?php if ( !empty( $modal['title'] ) || !empty( $modal['content'] ) || !empty( $modal['price'] ) || !empty( $modal['link'] ) ): ?>
								<div class="row">
									<div class="col-sm-12 title">
										<h4 id="<?php echo $modal['key']; ?>Modal"><?php echo illdy_sanitize_html( $modal['title'] ) ?></h4>
									</div>
						        	<div class="col-sm-12 content">
						        		<?php echo illdy_sanitize_html( $modal['content'] ); ?>
						        	</div>
						        	<?php if ( !empty($modal['price']) && !empty($modal['link']) ): ?>
							        	<div class="col-sm-12 price">
							        		<?php echo illdy_sanitize_html( $modal['price'] ); ?>
							        		<a href="<?php echo esc_url( home_url ( '/' . $modal['link'] ) ); ?>" class="btn btn-primary detail"><?php echo __( 'Detalhes', 'illdy' ); ?> <i class="fa fa-chevron-right"></i></a>
							        	</div>
						        	<?php endif; ?>
						        </div>
					    	<?php endif; ?>
					        <?php 
					        	$i = 0;
					        	while ( isset( $submodals[$modal['order']][ $i ] ) ): ?>
					        	<div class="row subcontent">
									<div class="col-sm-12 title">
					        			<h4><?php echo illdy_sanitize_html( $submodals[$modal['order']][ $i ][ 'title' ] ); ?></h4>
					        		</div>
						        	<div class="col-sm-12 content">
						        		<?php echo illdy_sanitize_html( $submodals[$modal['order']][ $i ][ 'content' ] ); ?>
						        	</div>
						        	<?php if ( !empty($submodals[$modal['order']][$i]['price']) && !empty($submodals[$modal['order']][$i]['link']) ): ?>
							        	<div class="col-sm-12 price">
							        		<?php echo illdy_sanitize_html( $submodals[$modal['order']][ $i ][ 'price' ] ); ?>
							        		<a href="<?php echo esc_url( home_url ( '/' . $submodals[$modal['order']][ $i ][ 'link' ] ) ); ?>" class="btn btn-primary detail"><?php echo __( 'Detalhes', 'illdy' ); ?> <i class="fa fa-chevron-right"></i></a>
							        	</div>
						        	<?php endif; ?>
						        </div>
					    	<?php
					    		$i++; 
					    		endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>

<?php endif; ?>

