<?php

require_once __DIR__.'/vendor/jangobrick/php-svg/autoloader.php';

/**
 *  Styles
 */
function juquitiba_turismo_enqueue_styles() {

    // Google Fonts
    $google_fonts_args = array(
        'family' => 'Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i'
    );

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    // Overwrite google fonts
    wp_register_style( 'illdy-google-fonts', add_query_arg( $google_fonts_args, 'https://fonts.googleapis.com/css' ), array(), null );
    wp_enqueue_style( 'illdy-google-fonts' );
    
}
add_action( 'wp_enqueue_scripts', 'juquitiba_turismo_enqueue_styles' );

/**
 *  Scripts
 */
function juquitiba_turismo_enqueue_javascripts() {
    // wp_enqueue_script( 'instafeed', get_stylesheet_directory_uri() . '/layout/js/instafeed/instafeed.js', array(), null, false);
}
add_action( 'wp_enqueue_scripts', 'juquitiba_turismo_enqueue_javascripts' );

/**
 *  Customizer
 */
require get_stylesheet_directory() . '/inc/customizer/customizer_juquitiba_turismo.php';

/**
 *  Overwrite: inc/extras.php
 *  Sections order
 */
if( !function_exists( 'illdy_sections_order' ) ) {
    function illdy_sections_order( $input ) {
        $about_general_show = get_theme_mod( 'illdy_about_general_show', 0 );
        $projects_general_show = get_theme_mod( 'illdy_projects_general_show', 0 );
        $testimonials_general_show = get_theme_mod( 'illdy_testimonials_general_show', 0 );
        $services_general_show = get_theme_mod( 'illdy_services_general_show', 1 );
        $latest_news_general_show = get_theme_mod( 'illdy_latest_news_general_show', 0 );
        $counter_general_show = get_theme_mod( 'illdy_counter_general_show', 0 );
        $team_general_show = get_theme_mod( 'illdy_team_general_show', 0 );
        $contact_us_general_show = get_theme_mod( 'illdy_contact_us_general_show', 0 );
        if( $input == 1 && $about_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'about' );
        } elseif( $input == 2 && $projects_general_show == 1 && illdy_is_active_jetpack_projects() ) {
            get_template_part( 'sections/front-page', 'projects' );
        } elseif( $input == 3 && $testimonials_general_show == 1 && illdy_is_active_jetpack_testimonials() ) {
            get_template_part( 'sections/front-page', 'testimonials' );
        } elseif( $input == 4 && $services_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'services' );
        } elseif( $input == 5 && $latest_news_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'latest-news' );
        } elseif( $input == 6 && $counter_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'counter' );
        } elseif( $input == 7 && $team_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'team' );
        } elseif( $input == 8 && $contact_us_general_show == 1 ) {
            get_template_part( 'sections/front-page', 'contact-us' );
        }
    }
}

/**
 *  Subcontent modals order
 */
if( !function_exists( 'juquitiba_turismo_subcontent_modals_order' ) ) {
    function juquitiba_turismo_subcontent_modals_order( $input ) {

        $submodals = array();

        $i = 1;
        $index = get_theme_mod( $input . $i );
        while ( !empty( $index ) ) {
            if ( !isset( $submodals[ $index ] ) ) $submodals[ $index ] = array();

            $aux = array(
                'title'   => get_theme_mod( $input . $i . '_title' ),
                'price'   => get_theme_mod( $input . $i . '_price' ),
                'link'    => get_theme_mod( $input . $i . '_link' ),
                'content' => get_theme_mod( $input . $i . '_content' )
            );
            array_push( $submodals[ $index ], $aux );
            $i++;
            $index = get_theme_mod( $input . $i );
        }

        return $submodals;
    }
}

/**
 *  Transform an absolute path to relative for SVGImage::fromFile function
 */
if( !function_exists( 'juquitiba_turismo_get_relative_path' ) ) {
    function juquitiba_turismo_get_relative_path($from, $to) {

        if (empty($from) || empty($to)) return '';

        // some compatibility fixes for Windows paths
        $from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
        $to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
        $from = str_replace('\\', '/', $from);
        $to   = str_replace('\\', '/', $to);

        $from     = explode('/', $from);
        $to       = explode('/', $to);
        $relPath  = $to;

        foreach($from as $depth => $dir) {
            // find first non-matching dir
            if($dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }
}



