FROM wordpress:4.7-php5.6-apache

RUN a2enmod rewrite && \
    apt-get update && \
    apt-get -y install software-properties-common && \
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
	apt-get install -y git && \
	docker-php-ext-install zip

# Set extra PHP.ini settings
RUN { \
        echo 'date.timezone="America/Sao_Paulo"'; \
        echo 'log_errors=Off'; \
        echo 'max_execution_time=400'; \
        echo 'max_input_vars=10000'; \
        echo 'max_input_time=400'; \
        echo 'memory_limit=256M'; \
        echo 'post_max_size=5120K'; \
        echo 'upload_max_filesize=5120K'; \
    } > /usr/local/etc/php/conf.d/php-extra.ini

ADD .ssh/ /root/.ssh
RUN chmod 400 /root/.ssh/*
RUN ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts
RUN mkdir /srv/www && \
    cd /srv/www && \
	git init && \
	git remote add origin git@bitbucket.org:pedrogoncalvesk/juquitiba-turismo.git && \
	git config core.sshCommand "ssh -i /root/.ssh/id_rsa -F /dev/null" && \
	git fetch && \
	git checkout master

COPY entrypoint-juqturis /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint-juqturis && \
    echo "exec 'entrypoint-juqturis'" >> /usr/local/bin/docker-entrypoint.sh